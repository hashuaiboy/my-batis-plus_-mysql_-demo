package com.example.mybatisplusdemo.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mybatisplusdemo.pojo.Employee;

public interface EmployeeService extends IService<Employee> {

}
