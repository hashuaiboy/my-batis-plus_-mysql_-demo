package com.example.mybatisplusdemo.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mybatisplusdemo.pojo.Admin;
import org.apache.ibatis.annotations.Select;

public interface AdminService extends IService<Admin> {
    Boolean checktoken(String str);
}
