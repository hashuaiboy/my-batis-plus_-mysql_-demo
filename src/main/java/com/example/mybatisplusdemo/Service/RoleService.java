package com.example.mybatisplusdemo.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mybatisplusdemo.pojo.Role;

public interface RoleService extends IService<Role> {

}
