package com.example.mybatisplusdemo.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mybatisplusdemo.Service.RoleService;
import com.example.mybatisplusdemo.mapper.RoleMapper;
import com.example.mybatisplusdemo.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
