package com.example.mybatisplusdemo.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mybatisplusdemo.Service.AdminService;
import com.example.mybatisplusdemo.mapper.AdminMapper;
import com.example.mybatisplusdemo.pojo.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {
    @Autowired
    AdminService adminService;

    /**
     * 判断token是否合法
     * @param str
     * @return Boolean
     */
    @Override
    public Boolean checktoken(String str) {
        List<Admin> tokens = adminService.list(new QueryWrapper<Admin>().select("token"));
        for (Admin t:tokens ){
            if (t.getToken().equals(str)){
                return true;
            }
        }
        return false;
    }
}
