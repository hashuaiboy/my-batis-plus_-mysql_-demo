package com.example.mybatisplusdemo.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mybatisplusdemo.Service.EmployeeService;
import com.example.mybatisplusdemo.mapper.EmployeeMapper;
import com.example.mybatisplusdemo.pojo.Employee;
import org.springframework.stereotype.Service;

/**
 * Service 继承mp提供的通用的ServiceImpl基类
 * 一个mapper 一个pojo
 */
@Service
public class EmployeeImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {

}
