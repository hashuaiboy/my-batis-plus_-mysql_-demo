package com.example.mybatisplusdemo.Controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mybatisplusdemo.Result.Result;
import com.example.mybatisplusdemo.Service.AdminService;
import com.example.mybatisplusdemo.Service.RoleService;
import com.example.mybatisplusdemo.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RoleController {
    @Autowired
    AdminService adminService;
    @Autowired
    RoleService roleService;

    /**
     * 得到所有的权限信息
     * @param token
     * @return json
     */
    @RequestMapping(value = "/getallroles",method = RequestMethod.GET)
    public Result getallroles(@RequestParam("token") String token){
        if (!adminService.checktoken(token)) {
            return new Result(400,"没有权限！");
        }
        List<Role> rolelist = roleService.list();
        return new Result(200,"获取数据成功！",rolelist);
    }

    @RequestMapping(value = "/addrole/{token}",method = RequestMethod.POST)
    public Result addrole(@RequestBody Role role,@PathVariable("token") String token){
        if (!adminService.checktoken(token)) {
            return new Result(400,"没有权限！");
        }
        int roleName = roleService.count(new QueryWrapper<Role>().eq("roleName", role.getRoleName()));
        if (roleName != 0){
            return new Result(400,"身份已经存在！");
        }
        roleService.save(role);
        return new Result(200,"添加成功！",role);
    }
}
