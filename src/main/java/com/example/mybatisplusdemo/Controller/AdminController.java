package com.example.mybatisplusdemo.Controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mybatisplusdemo.Result.Result;
import com.example.mybatisplusdemo.Service.AdminService;
import com.example.mybatisplusdemo.Service.RoleService;
import com.example.mybatisplusdemo.pojo.Admin;
import com.example.mybatisplusdemo.pojo.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AdminController {
    @Autowired
    AdminService adminService;
    @Autowired
    RoleService roleService;

    /**
     * 得到所有的admin信息
     * @param token
     * @return json
     */
    @RequestMapping(value = "/getalladmins",method = RequestMethod.GET)
    public Result getalladmins(@RequestParam("token") String token){
        if (!adminService.checktoken(token)) {
            return new Result(400,"没有权限！");
        }
        List<Role> rolelist = roleService.list();
        List<Admin> adminlist = adminService.list();
        for (Admin a:adminlist){
            for(Role r:rolelist){
                if (a.getRoleId().equals(r.getId())) {a.setRolename(r.getRoleName());}
            }
        }
        return new Result(200,"获取数据成功！",adminlist);
    }

    /**
     * 根据ID 查询admin
     * @param token
     * @param id
     * @return json
     */
    @RequestMapping(value = "/getadminById",method = RequestMethod.GET)
    public Result getadminByid(@RequestParam("token") String token,@RequestParam("id") Integer id){
        if (!adminService.checktoken(token)) {
            return new Result(400,"没有权限！");
        }
        Admin admin = adminService.getOne(new QueryWrapper<Admin>().eq("id", id));
        Role one = roleService.getOne(new QueryWrapper<Role>().select("roleName").eq("id", admin.getRoleId()));
        admin.setRolename(one.getRoleName());
        return new Result(200,"查询成功",admin);
    }

    /**
     * 根据 用户名模糊查询
     * @return json
     */
    @RequestMapping(value = "/getadminByName",method = RequestMethod.GET)
    public Result getadminByName(@RequestParam("token") String token,@RequestParam("username") String username){
        if (!adminService.checktoken(token)) {
            return new Result(400,"没有权限！");
        }
        List<Admin> adminlist = adminService.list(new QueryWrapper<Admin>().like("username", username));
        List<Role> rolelist = roleService.list();
        for (Admin a:adminlist){
            for (Role r:rolelist){
                if (a.getRoleId().equals(r.getId())){
                    a.setRolename(r.getRoleName());
                }
            }
        }
        return new Result(200,"获取数据成功！",adminlist);
    }

    /**
     * 添加admin用户    [未完善]
     * @param admin
     * @param token
     * @return json
     */
    @RequestMapping(value = "/addadmin/{token}",method = RequestMethod.POST)
    public Result addadmin(@RequestBody Admin admin,@PathVariable("token") String token){
        if (!adminService.checktoken(token)) {
            return new Result(400,"没有权限！");
        }
        int username = adminService.count(new QueryWrapper<Admin>().eq("username", admin.getUsername()));
        if ( username !=0 ){
            return new Result(400,"已经存在这个用户！");
        }
        return new Result(200,"测试添加成功",admin);
    }

    @GetMapping("/admininfo")
    public Result getAdminInfo(){
        List<Admin> list = adminService.list(new QueryWrapper<Admin>().last("left join role on admin.roleId = role.id").select("admin.id","roleName"));
        return new Result(200,"ok",list);
    }
}
