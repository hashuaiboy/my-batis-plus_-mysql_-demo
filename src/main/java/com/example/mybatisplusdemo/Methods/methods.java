package com.example.mybatisplusdemo.Methods;


import com.example.mybatisplusdemo.Service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class methods {
    /**
     * md5 加密
     * @param str
     * @return String
     */
    public String md5(String str){
        String md5 = "";
        md5 = DigestUtils.md5DigestAsHex(str.getBytes(StandardCharsets.UTF_8));
        return md5;
    }

    /**
     * 生成指定长度随机字符串
     * @param length
     * @return string
     */
    public String randomstr(int length){
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer stringBuffer = new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            stringBuffer.append(str.charAt(number));
        }
        return stringBuffer.toString();
    }

}
