package com.example.mybatisplusdemo;

import com.example.mybatisplusdemo.Service.RoleService;
import com.example.mybatisplusdemo.pojo.Role;
import org.junit.jupiter.api.Test;
import com.example.mybatisplusdemo.Methods.methods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
@SpringBootTest
public class Methodstest {
    @Autowired
    RoleService roleService;

    @Test
    void randomstr(){
        methods methods = new methods();
        System.out.println(methods.randomstr(14));
    }

    @Test
    void checktoken(){
        List<Role> list = roleService.list();
        list.forEach(System.out::println);
    }
}
