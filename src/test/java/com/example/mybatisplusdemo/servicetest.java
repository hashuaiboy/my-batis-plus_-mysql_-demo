package com.example.mybatisplusdemo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mybatisplusdemo.Service.AdminService;
import com.example.mybatisplusdemo.Service.EmployeeService;
import com.example.mybatisplusdemo.pojo.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
public class servicetest {
    @Autowired
    EmployeeService employeeService;
    @Autowired
    AdminService adminService;

    @Test
    void query(){
        Employee byId = employeeService.getById(1);
        System.out.println(byId);
    }

    @Test
    void queryall(){
        List<Employee> list = employeeService.list();
        System.out.println(list);
    }

    @Test
    void queryonebymap(){
        HashMap<String, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("age",18);
        List<Employee> list = employeeService.listByMap(objectObjectHashMap);
        System.out.println(list);
    }

    @Test
    void nornaltest(){
        System.out.println(adminService.checktoken("1j9zZZfgaUoBJo"));
    }

    @Test
    void addlist() {
        Employee employee1 = new Employee();
        Employee employee2 = new Employee();
        Employee employee3 = new Employee();
        employee1.setUsername("test1");employee1.setAge(10);employee1.setEmail("test@test.com");
        employee2.setUsername("test1");employee2.setAge(10);employee2.setEmail("test@test.com");
        employee3.setUsername("test1");employee3.setAge(10);employee3.setEmail("test@test.com");
        Collection collection =new ArrayList<>();
        collection.add(employee1);
        collection.add(employee2);
        collection.add(employee3);
        boolean b = employeeService.saveBatch(collection);
        System.out.println(b);
    }
}
