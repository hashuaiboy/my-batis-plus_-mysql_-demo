package com.example.mybatisplusdemo;


import com.example.mybatisplusdemo.Methods.methods;
import com.example.mybatisplusdemo.mapper.EmployeeMapper;
import com.example.mybatisplusdemo.pojo.Employee;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;


@SpringBootTest
class MyBatisPlusDemoApplicationTests {
    @Autowired
    EmployeeMapper employeeMapper;

    @Test
    void query() {
        List<Employee> employees = employeeMapper.selectList(null);
        System.out.println(employees);
    }

    @Test
    void add(){
        Employee employee = new Employee();
        employee.setId(null);
        employee.setUsername("胡国栋");
        employee.setAge(99);
        employee.setEmail("666@666.com");
        employeeMapper.insert(employee);
    }

    @Test
    void update(){
        Employee employee = new Employee();
        employee.setId(6);
        employee.setUsername("胡国栋");
        employee.setAge(100);
        employee.setEmail("666@666.com");
        employeeMapper.updateById(employee);
        System.out.println(employee);
    }

    @Test
    void delete(){
        employeeMapper.deleteById(6);
    }

    @Test
    void list(){
        List<Employee> employees = employeeMapper.selectList(null);
        System.out.println(employees);
    }

    @Test
    void listmapper(){
        // 按照mapper查询
        HashMap<String, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("age",18);
        List<Employee> employees = employeeMapper.selectByMap(objectObjectHashMap);
        System.out.println(employees);
    }

}
