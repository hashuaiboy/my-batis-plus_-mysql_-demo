## API接口文档

## 1.请求路径

baseurl:http://localhost:8081/api

## 2.请求列表

#### admin 登录接口

请求url：/login

所需参数 @RequestBody Admin admin

实体类 admin的所有参数

###### 返回值实例

请求成功

```json
code:200,
msg:"登录成功",
data:略
```

请求失败

```json
code:400,
msg:"登录失败"
```

#### 查询所有admin

请求url：/getalladmins

所需参数 token

###### 返回值实例

请求成功

```json
code:200,
msg:"获取数据成功",
data:略
```

请求失败

```json
code:400,
msg:"获取数据失败"
```

#### 根据id查找admin

请求url：/getadminById

所需参数 interger id ,String token

###### 返回值实例

略

#### 模糊查找admin

请求url：/getadminByName

所需参数 String token,String username

###### 返回值实例

略

#### 查询所有员工

请求url：/getemployees

所需参数 String token

###### 返回值实例

略



#### 添加员工

请求url：/addemployee

所需参数 Employee类的所有参数 , String token

###### 返回值实例

略

#### 查询所有身份

请求url：/getallroles

所需参数 String token

###### 返回值实例

略

#### 添加role

请求url：/addadmin/{token} + role



#### 添加admin用户

待开发

# 增加中....



